<?php
    class Book {
        private $db;

        public function __construct(){
            $this->db = new Database;
        }

        public function getBooks(){
            $this->db->query('SELECT *, books.id as bookId, users.id as userId FROM books INNER JOIN users ON books.user_id = users.id ORDER BY pub_year DESC');

            $results = $this->db->resultSet();
            
            return $results;
        }

        // Add Books
        public function addBook($data){
            $this->db->query('INSERT INTO books (title, description, author, pub_year, ISBN, user_id) VALUES(:title, :description, :author, :pub_year, :ISBN, :user_id)');
            // Bind values
            $this->db->bind(':title', $data['title']);
            $this->db->bind(':description', $data['description']);
            $this->db->bind(':author', $data['author']);
            $this->db->bind(':pub_year', $data['pub_year']);
            $this->db->bind(':ISBN', $data['ISBN']);
            $this->db->bind(':user_id', $data['user_id']);
      
            // Execute
            if($this->db->execute()){
              return true;
            } else {
              return false;
            }
        }

        // Update Books
        public function updateBook($data){
            $this->db->query('UPDATE books SET title = :title, description = :description, author = :author, pub_year = :pub_year, ISBN = :ISBN WHERE id = :id');
            // Bind values
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':title', $data['title']);
            $this->db->bind(':description', $data['description']);
            $this->db->bind(':author', $data['author']);
            $this->db->bind(':pub_year', $data['pub_year']);
            $this->db->bind(':ISBN', $data['ISBN']);
      
            // Execute
            if($this->db->execute()){
              return true;
            } else {
              return false;
            }
        }

        // Connects with line ... in Books.php (controller)
        public function getBookById($id){
            $this->db->query('SELECT * FROM books WHERE id = :id');
            $this->db->bind(':id', $id);

            $row = $this->db->single();

            return $row;
        }

        // Connects with line ... from the model Book.php (model)
        public function deleteBook($id){
          $this->db->query('DELETE FROM books WHERE id = :id');
            // Bind values
            $this->db->bind(':id', $id);
      
            // Execute
            if($this->db->execute()){
              return true;
            } else {
              return false;
            }
        }
    }