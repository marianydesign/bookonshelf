<?php require APPROOT . '/views/inc/header.php'; ?>
    <?php flash('post_message'); ?>
    <div class="row mb-3">
        <div class="col-md-6">
            <h1>Books</h1>
        </div>
        <div class="col-md-6">
            <a href="<?php echo URLROOT; ?>/books/add/" class="btn btn-primary pull-right">
                <i class="fa fa-pencil"></i> Add book
            </a>
        </div>
    </div>
    <!-- Fetch books from db -->
    <?php foreach($data['books'] as $book) : ?>
        <div class="card card-body mb-3">
            <h4 class="card-title"><?php echo $book->title; ?></h4>
                <div class="bg-light p-2 mb-3">
                    Added by <?php echo $book->first_name; ?> <?php echo $book->sur_name; ?>
                </div>
                <p class="card-text"><?php echo $book->description; ?></p>
                <a href="<?php echo URLROOT; ?>/books/show/<?php echo $book->bookId; ?>" class="btn btn-dark">More</a>
        </div>
    <?php endforeach; ?>
<?php require APPROOT . '/views/inc/footer.php'; ?>