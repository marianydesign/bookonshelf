<?php require APPROOT . '/views/inc/header.php'; ?>
  <a href="<?php echo URLROOT; ?>/books" class="btn btn-light"><i class="fa fa-backward"></i>  Back</a>
      <div class="card card-body bg-light mt-5">
        <h2>Add Book</h2>
        <p>Add a book with this form</p>
        <form action="<?php echo URLROOT; ?>/books/add" method="post">
          <div class="form-group">
            <label for="title">Title: <sup>*</sup></label>
            <input type="text" name="title" class="form-control form-control-lg <?php echo (!empty($data['title_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['title']; ?>">
            <span class="invalid-feedback"><?php echo $data['title_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="description">Description: <sup>*</sup></label>
            <textarea name="description" class="form-control form-control-lg <?php echo (!empty($data['description_err'])) ? 'is-invalid' : ''; ?>"><?php echo $data['description']; ?></textarea>
            <span class="invalid-feedback"><?php echo $data['description_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="author">Author: <sup>*</sup></label>
            <textarea name="author" class="form-control form-control-lg <?php echo (!empty($data['author_err'])) ? 'is-invalid' : ''; ?>"><?php echo $data['author']; ?></textarea>
            <span class="invalid-feedback"><?php echo $data['author_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="pub_year">Publication Year: <sup>*</sup></label>
            <textarea name="pub_year" class="form-control form-control-lg <?php echo (!empty($data['pub_year_err'])) ? 'is-invalid' : ''; ?>"><?php echo $data['pub_year']; ?></textarea>
            <span class="invalid-feedback"><?php echo $data['pub_year_err']; ?></span>
          </div>
          <div class="form-group">
            <label for="isbn">ISBN: <sup>*</sup></label>
            <textarea name="ISBN" class="form-control form-control-lg <?php echo (!empty($data['ISBN_err'])) ? 'is-invalid' : ''; ?>"><?php echo $data['ISBN']; ?></textarea>
            <span class="invalid-feedback"><?php echo $data['ISBN_err']; ?></span>
          </div>
          <input type="submit" class="btn btn-success" value="Submit">
        </form>
      </div>
    
<?php require APPROOT . '/views/inc/footer.php'; ?>