<?php require APPROOT . '/views/inc/header.php'; ?>
    <a href="<?php echo URLROOT; ?>/books" class="btn btn-light"><i class="fa fa-backward"></i>  Back</a>
    <br>
    <h1><?php echo $data['book']->title; ?></h1>
    <div class="bg-secondary text-white p-2 mb-3">
        Added by <?php echo $data['user']->first_name; ?> <?php echo $data['user']->sur_name; ?>
    </div>
    <p><?php echo $data['book']->description; ?></p>
    <hr>
    <p><?php echo $data['book']->author; ?></p>
    <hr>
    <p><?php echo $data['book']->pub_year; ?></p>
    <hr>
    <p><?php echo $data['book']->ISBN; ?></p>

    <!-- Check which User is logged in to show Edit button yes/no -->
    <?php if($data['book']->user_id == $_SESSION['user_id']) : ?>
        <hr>
        <a href="<?php echo URLROOT; ?>/books/edit/<?php echo $data['book']->id; ?>" class="btn btn-dark">Edit</a>

        <form class="pull-right" action="<?php echo URLROOT; ?>/books/delete/<?php echo $data['book']->id; ?>" method="post">
            <input type="submit" value="Delete" class="btn btn-danger">
        </form>
<?php endif; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>