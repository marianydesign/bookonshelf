<?php
  // DB Params
  define('DB_HOST', 'localhost');
  define('DB_USER', 'root');
  define('DB_PASS', '');
  define('DB_NAME', 'BookOnShelf');

  // App Root
  define('APPROOT', dirname(dirname(__FILE__)));
  // URL Root
  define('URLROOT', 'http://localhost/bookonshelf');
  // Site Name
  define('SITENAME', 'BookOnShelf');
  // App Version
  define('APPVERSION', '1.0.0');