<?php
    class Books extends Controller {
        public function __construct(){
            // Check if logged in
            if(!isLoggedIn()){
                redirect('users/login');
            }

            $this->bookModel = $this->model('Book');
            $this->userModel = $this->model('User');
        }

        public function index(){
            // Get books
            $books = $this->bookModel->getBooks();

            $data = [
                'books' => $books
            ];

            $this->view('books/index', $data);
        }

        // Add books
        public function add(){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                // Sanitize POST array
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data = [
                    'title' => trim($_POST['title']),
                    'description' => trim($_POST['description']),
                    'author' => trim($_POST['author']),
                    'pub_year' => trim($_POST['pub_year']),
                    'ISBN' => trim($_POST['ISBN']),
                    'user_id' => $_SESSION['user_id'],
                    'title_err' => '',
                    'description_err' => '',
                    'author_err' => '',
                    'pub_year_err' => '',
                    'ISBN_err' => ''
                ];

                // Validate data
                if(empty($data['title'])){
                    $data['title_err'] = 'Please enter title';
                }
                if(empty($data['description'])){
                    $data['description_err'] = 'Please enter a description';
                }
                if(empty($data['author'])){
                    $data['author_err'] = 'Please enter an author';
                }
                if(empty($data['pub_year'])){
                    $data['pub_year_err'] = 'Please enter a year of publication';
                }
                if(empty($data['ISBN'])){
                    $data['ISBN_err'] = 'Please enter an ISBN';
                }

                // Make sure no errors 
                if(empty($data['title_err']) && empty($data['description_err']) && empty($data['author_err']) && empty($data['pub_year_err']) && empty($data['ISBN_err'])){
                    // Validated
                    if($this->bookModel->addBook($data)){
                        flash('post_message', 'Book Added');
                        redirect('books');
                    } else {
                        die('Something went wrong');
                    }
                } else {
                    // Load view with errors
                    $this->view('books/add', $data);
                }

            } else {

                $data = [
                    'title' => '',
                    'description' => '',
                    'author' => '',
                    'pub_year' => '',
                    'ISBN' => ''
                ];

                $this->view('books/add', $data);
            }
        }

        // Edit posts
        public function edit($id){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                 // Sanitize POST array
                 $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
                    $data = [
                        'id' => $id,
                        'title' => trim($_POST['title']),
                        'description' => trim($_POST['description']),
                        'author' => trim($_POST['author']),
                        'pub_year' => trim($_POST['pub_year']),
                        'ISBN' => trim($_POST['ISBN']),
                        'user_id' => $_SESSION['user_id'],
                        'title_err' => '',
                        'description_err' => '',
                        'author_err' => '',
                        'pub_year_err' => '',
                        'ISBN_err' => ''
                    ];
        
                        // Validate data
                        if(empty($data['title'])){
                            $data['title_err'] = 'Please enter title';
                        }
                        if(empty($data['description'])){
                            $data['description_err'] = 'Please enter a description';
                        }
                        if(empty($data['author'])){
                            $data['author_err'] = 'Please enter an author';
                        }
                        if(empty($data['pub_year'])){
                            $data['pub_year_err'] = 'Please enter a year of publication';
                        }
                        if(empty($data['ISBN'])){
                            $data['ISBN_err'] = 'Please enter an ISBN';
                        }
        
                        // Make sure no errors 
                        if(empty($data['title_err']) && empty($data['description_err']) && empty($data['author_err']) && empty($data['pub_year_err']) && empty($data['ISBN_err'])){

                            // Validated
                            if($this->bookModel->updateBook($data)){
                                flash('post_message', 'Book Updated');
                                redirect('books');
                            } else {
                                die('Something went wrong');
                            }
                        } else {
                            // Load view with errors
                            $this->view('books/edit', $data);
                        }
        
                    } else {
                        // Get existing book from model
                        $book = $this->bookModel->getBookByID($id);
                        // Check for owner
                        if($book->user_id != $_SESSION['user_id']){
                            redirect('books');
                        }

                        // Fetch data from DB to show inside the form where you edit in
                        $data = [
                            'id' => $id,
                            'title' => $book->title,
                            'description' => $book->description,
                            'author' => $book->author,
                            'pub_year' => $book->pub_year,
                            'ISBN' => $book->ISBN
                        ];
        
                        $this->view('books/edit', $data);
                    }
                }            

        // Connects with line ... in Book.php (model)
        public function show($id){
            $book = $this->bookModel->getBookById($id);
            $user = $this->userModel->getUserById($book->user_id);
            
            $data = ['book' => $book,
                     'user' => $user
            ];

            $this->view('books/show', $data);
        }

        // First make a controller function here and than go and add lines to the model
        public function delete($id){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                // Get existing book from model
                $post = $this->bookModel->getBookByID($id);
                
                // Check for owner (if not than redirect!)
                if($post->user_id != $_SESSION['user_id']){
                    redirect('books');
                }
                if($this->bookModel->deleteBook($id)){
                    flash('post_message', 'Book Removed');
                } else {
                    die('Something went wrong');
                } 
            } 
            // redirect after book has been edited by correct user
            redirect('books');
        }
    }