<?php
    class Posts extends Controller {
        public function __construct(){
            // Check if logged in
            if(!isLoggedIn()){
                redirect('users/login');
            }

            $this->postModel = $this->model('Post');
            $this->userModel = $this->model('User');
        }

        public function index(){
            // Get posts
            $posts = $this->postModel->getPosts();

            $data = [
                'posts' => $posts
            ];

            $this->view('posts/index', $data);
        }

        // Add posts
        public function add(){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                // Sanitize POST array
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data = [
                    'title' => trim($_POST['title']),
                    'body' => trim($_POST['body']),
                    'user_id' => $_SESSION['user_id'],
                    'title_err' => '',
                    'body_err' => ''
                ];

                // Validate data
                if(empty($data['title'])){
                    $data['title_err'] = 'Please enter title';
                }
                if(empty($data['body'])){
                    $data['body_err'] = 'Please enter body';
                }

                // Make sure no errors 
                if(empty($data['title_err']) && empty($data['body_err'])){
                    // Validated
                    if($this->postModel->addPost($data)){
                        flash('post_message', 'Post Added');
                        redirect('posts');
                    } else {
                        die('Something went wrong');
                    }
                } else {
                    // Load view with errors
                    $this->view('posts/add', $data);
                }

            } else {

                $data = [
                    'title' => '',
                    'body' => ''
                ];

                $this->view('posts/add', $data);
            }
        }

        // Edit posts
        public function edit($id){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                 // Sanitize POST array
                 $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
                    $data = [
                         'id' => $id,
                         'title' => trim($_POST['title']),
                          'body' => trim($_POST['body']),
                          'user_id' => $_SESSION['user_id'],
                          'title_err' => '',
                          'body_err' => ''
                        ];
        
                        // Validate data
                        if(empty($data['title'])){
                            $data['title_err'] = 'Please enter title';
                        }
                        if(empty($data['body'])){
                            $data['body_err'] = 'Please enter body';
                        }
        
                        // Make sure no errors 
                        if(empty($data['title_err']) && empty($data['body_err'])){
                            // Validated
                            if($this->postModel->updatePost($data)){
                                flash('post_message', 'Post Updated');
                                redirect('posts');
                            } else {
                                die('Something went wrong');
                            }
                        } else {
                            // Load view with errors
                            $this->view('posts/edit', $data);
                        }
        
                    } else {
                        // Get existing post from model
                        $post = $this->postModel->getPostByID($id);
                        // Check for owner
                        if($post->user_id != $_SESSION['user_id']){
                            redirect('posts');
                        }

                        // Fetch data from DB to show inside the form where you edit in
                        $data = [
                            'id' => $id,
                            'title' => $post->title,
                            'body' => $post->body
                        ];
        
                        $this->view('posts/edit', $data);
                    }
                }            

        // Connects with line 32 in Post.php (model)
        public function show($id){
            $post = $this->postModel->getPostById($id);
            $user = $this->userModel->getUserById($post->user_id);
            
            $data = ['post' => $post,
                     'user' => $user
            ];

            $this->view('posts/show', $data);
        }

        // First make a controller function here and than go and add lines to the model
        public function delete($id){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                // Get existing post from model
                $post = $this->postModel->getPostByID($id);
                
                // Check for owner (if not than redirect!)
                if($post->user_id != $_SESSION['user_id']){
                    redirect('posts');
                }
                if($this->postModel->deletePost($id)){
                    flash('post_message', 'Post Removed');
                } else {
                    die('Something went wrong');
                } 
            } 
            // redirect after post has been edited by correct user
            redirect('posts');
        }
    }