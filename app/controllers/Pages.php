<?php
  class Pages extends Controller {
    public function __construct(){
     
    }
    
    // Where directed to after logging in
    public function index(){
      if(isLoggedIn()){
        redirect('books');
      }

      $data = [
        'title' => 'BookOnShelf',
        'description' => 'Get Your Book From Our Shelf!'
      ];
     
      $this->view('pages/index', $data);
    }

    public function about(){
      $data = [
        'title' => 'About Us',
        'description' => 'Get Your Book From Our Shelf!'
      ];

      $this->view('pages/about', $data);
    }
  }